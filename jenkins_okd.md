# Setup Jenkins
1\. Install Argo CD operator

`Operators - OperatorHub - Developer Catalog - Filter : Jenkins`

- Namespace : jenkins
- Jenkins Service Name : jenkins
- Jenkins JNLP Service Name : jenkins-jnlp

2\. Access jenkins dashboard

`Networking - Routes`

3\. Set Number of executors in Master node to 0

`Manage Jenkins - Manage Nodes and Clouds`

4\. Add new pod template for executor

`Manage Jenkins - Manage Nodes and Clouds - Configure Clouds - Pod Templates... - Add Pod Template`

- Name: webapp-agent
- Labels: webapp-agent

`Add Container`

- Name: openshift-client
- Docker image: quay.io/atwatanmalikm/openshift-client:v1
- Always pull image: checked
- Working directory: /home/jenkins/agent

`Add Container`

- Name: webapp-agent
- Docker image: quay.io/atwatanmalikm/webapp-agent:v1
- Always pull image: checked
- Working directory: /home/jenkins/agent

- Service Account: jenkins

`Save`

5\. Add rolebinding to service account jenkins (Execute in Bastion)
```bash
oc create rolebinding -n jenkins anyuid --clusterrole=system:openshift:scc:anyuid --serviceaccount=jenkins:jenkins
```

---

# Create Pipeline
1\. Login to GitHub (<https://github.com/>) and fork repo from <https://github.com/atwatanmalikm/simple-webapp>


2\. Create pipeline

`New Item - Pipeline`

- Name : simple-webapp

**Build Triggers**

- Poll SCM : H/2 * * * *

**Pipeline**

- Definition : Pipeline script from SCM
- SCM : Git
- Repository URL : https://github.com/<your_username>/simple-webapp.git
- Branch Specifier : */main
- Script Path : Jenkinsfile

3\. Clone your simple-webapp repository
```bash
git clone https://github.com/<your_username>/simple-webapp
cd simple-webapp
```

4\. Edit index.html in repo
```bash
cat html/index.html.backup2 > html/index.html
```

5\. Edit app version in Jenkinsfile
```bash
vim Jenkinsfile

environment {
        version = 'v1'
    }
```

6\. Push to repo
```bash
git add .
git commit -m "update files"
git push
```

7\. Open Blue Ocean and choose simple-webapp

8\. Run and open job

9\. When success until "Approval" step, click "Yes, deploy it"

10\. Open openshift dashboard, and access simple-webapp in project "jenkins"

`Networking - Routes`

11\. Edit index.html and Jenkinsfile
```bash
cat html/index.html.backup1 > html/index.html

vim Jenkinsfile

environment {
        version = 'v2'
    }
```

12\. Push to repo
```bash
git add .
git commit -m "update files"
git push
```

13\. Open Blue Ocean and choose simple-webapp (wait until new job is running)

14\. Open new job

15\. When success until "Approval" step, click "Yes, deploy it"

16\. Refresh simple-webapp page