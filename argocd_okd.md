## Setup ArgoCD
1\. Create new project named "**argocd**"

`Home - Projects - Create Project`

2\. Install Argo CD operator

`Operators - OperatorHub`

- Installation mode : A specific namespace on the cluster
- Installed Namespace : argocd
- Update approval: Automatic

3\. Create ArgoCD instance

- Name : argocd-instance

4\. Check running pods in argocd namespace

`Workloads - Pods`

5\. Show admin password

`Workload - Secrets - Filter: argocd-instance-cluster`

6\. Open ArgoCD dashboard

`Networking - Routes`

7\. Login using username **admin** and password from secret

8\. Update admin password to **adminpass** by edit secret **argocd-instance-cluster**

`Workload - Secrets - Filter: argocd-instance-cluster`

9\. Logout and Login with new password

10\. Install ArgoCD client in bastion node
```bash
curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64
chmod +x /usr/local/bin/argocd
```

11\. Login via client
```bash
argocd login argocd-instance-server-argocd.apps.openshift.podX.io
argocd account get-user-info
```

12\. Show available cluster to add
```bash
argocd cluster add
```

13\. Add okd cluster api
```bash
argocd cluster add default/api-openshift-podX-io:6443/admin
```

14\. Show cluster list
```bash
argocd cluster list
```

# Implementing GitOps using ArgoCD
1\. Create new argocd project
```bash
argocd proj create simple-webapp --allow-cluster-resource=*/* --dest=*,* --src=*
```

2\. Show argocd project list
```bash
argocd proj list
```

3\. Login to GitHub ([https://github.com/](https://github.com/)) and fork repo from [https://github.com/atwatanmalikm/argo-exercise](https://github.com/atwatanmalikm/argo-exercise)

4\. Add repo to argocd
```bash
argocd repo add https://github.com/atwatanmalikm/argo-exercise.git --project simple-webapp
argocd repo list
```

5\. Open Argo CD dashboard and Create Application

**GENERAL**

- Application Name : simple-webapp
- Project : simple-webapp
- Sync Policy : Automatic

**SOURCE**

- Repository URL : https://github.com/atwatanmalikm/argo-exercise
- Path : .

**DESTINATION**

- Cluster URL : https://api.openshift.podX.io:6443
- Namespace : argo-apps

6\. Create and monitor until "Healthy and Synced"

7\. Find route URL in OKD dashboard
`Networking - Routes - Project: argo-apps**

8\. Access app using route URL

9\. Go to github repo and edit app version in manifest **nginx-argo-deployment.yaml**

Change container image to **quay.io/atwatanmalikm/simple-webapp:v2** and Commit changes

10\. Back to Argo CD dashboard and wait for auto sync (about 3 minutes)

11\. After synced, refresh the route URL page

Gihub Token : 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
