# Install Cluster OKD 4.9

# Pre Installation

1. Update and install packages

```bash
dnf update -y && dnf install -y epel-release
dnf install -y vim curl wget tmux
```

---

# Configure NTP Server

1. Install chrony package
    
    ```bash
    dnf install -y chrony
    ```
    
2. Configure chrony
    
    ```bash
    vim /etc/chrony.conf
    ```
    
    ```bash
    ...
    #pool [2.centos.pool.ntp.org](http://2.centos.pool.ntp.org/) iburst
    server [0.id.pool.ntp.org](http://0.id.pool.ntp.org/)
    server [1.id.pool.ntp.org](http://1.id.pool.ntp.org/)
    server [2.id.pool.ntp.org](http://2.id.pool.ntp.org/)
    server [3.id.pool.ntp.org](http://3.id.pool.ntp.org/)
    
    allow 192.168.1.0/24
    ...
    ```
    
3. Enable and restart service
    
    ```bash
    systemctl enable chronyd
    systemctl restart chronyd
    systemctl status chronyd
    ```
    
4. Verify NTP Server
    
    ```bash
    chronyc sources
    ```
    
5. Set timezone to Asia/Jakarta
    
    ```bash
    timedatectl set-timezone Asia/Jakarta
    timedatectl
    ```
    
6. Allow firewall
    
    ```bash
    firewall-cmd --permanent --add-service=ntp
    firewall-cmd --reload
    firewall-cmd --list-services
    ```
    
    ---
    

# Setup DNS Server

1. Install packages
    
    ```bash
    dnf install -y bind bind-utils
    ```
    
2. Define zone
    
    ```bash
    mv /etc/named.conf /etc/named.conf.bak
    vim /etc/named.conf
    ```
    
    ```bash
    ...
    options {
    directory   "/var/named";
    dump-file   "/var/named/data/cache_dump.db";
    statistics-file "/var/named/data/named_stats.txt";
    memstatistics-file "/var/named/data/named_mem_stats.txt";
    secroots-file   "/var/named/data/named.secroots";
    recursing-file  "/var/named/data/named.recursing";
    allow-query     { localhost;192.168.1.0/24; };
    listen-on port 53 { any; };
    
    /*
     - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
     - If you are building a RECURSIVE (caching) DNS server, you need to enable
       recursion.
     - If your recursive DNS server has a public IP address, you MUST enable access
       control to limit queries to your legitimate users. Failing to do so will
       cause your server to become part of large scale DNS amplification
       attacks. Implementing BCP38 within your network would greatly
       reduce such attack surface
    */
    recursion yes;
        forwarders {
                8.8.8.8;
                8.8.4.4;
        };
    dnssec-enable yes;
    dnssec-validation yes;
    
    managed-keys-directory "/var/named/dynamic";
    
    pid-file "/run/named/named.pid";
    session-keyfile "/run/named/session.key";
    
    /* <https://fedoraproject.org/wiki/Changes/CryptoPolicy> */
    include "/etc/crypto-policies/back-ends/bind.config";
    
    };
    
    logging {
    channel default_debug {
    file "data/named.run";
    severity dynamic;
    };
    };
    
    zone "." IN {
    type hint;
    file "[named.ca](http://named.ca/)";
    };
    
    zone "[openshift.podX.io](http://openshift.pod1.io/)" {
    type master;
    file "dynamic/forward.db";
    };
    
    zone "1.168.192.in-addr.arpa" {
    type master;
    file "dynamic/reverse.db";
    };
    
    include "/etc/named.rfc1912.zones";
    include "/etc/named.root.key";
    
    ...
    ```
    
3. Create forward zone file
    
    ```bash
    vim /var/named/dynamic/forward.db
    ```
    
    ```bash
    ...
    $TTL 1W
    @       IN      SOA     [ns1.openshift.podX.io](http://ns1.openshift.podx.io/). root (
    2020092301      ; serial
    3H              ; refresh (3 hours)
    30M             ; retry (30 minutes)
    2W              ; expiry (2 weeks)
    1W )            ; minimum (1 week)
    IN      NS      [ns1.openshift.podX.io](http://ns1.openshift.podx.io/).
    IN      MX 10   [smtp.openshift.podX.io](http://smtp.openshift.podx.io/).
    ;
    ;
    ns1     IN      A       192.168.1.X0
    smtp    IN      A       192.168.1.X0
    ;
    helper  IN      A       192.168.1.X0
    ;
    ; The api points to the IP of your load balancer
    api             IN      A       192.168.1.X0
    api-int         IN      A       192.168.1.X0
    ;
    ; The wildcard also points to the load balancer
    *.apps          IN      A       192.168.1.X0
    ;
    ; Create entry for the bootstrap host
    bootstrap       IN      A       192.168.1.X1
    ;
    ; Create entries for the master hosts
    master1         IN      A       192.168.1.X2
    master2         IN      A       192.168.1.X3
    master3         IN      A       192.168.1.X4
    ;
    ; Create entries for the worker hosts
    worker1         IN      A       192.168.1.X5
    worker2         IN      A       192.168.1.X6
    ;
    ...
    ```
    
4. Create reverse zone file
    
    ```bash
    vim /var/named/dynamic/reverse.db
    ```
    
    ```bash
    ...
    $TTL 1W
    @       IN      SOA     [ns1.openshift.podX.io](http://ns1.openshift.podx.io/). root (
    2020092301      ; serial
    3H              ; refresh (3 hours)
    30M             ; retry (30 minutes)
    2W              ; expiry (2 weeks)
    1W )            ; minimum (1 week)
    IN      NS      [ns1.openshift.podX.io](http://ns1.openshift.podx.io/).
    ;
    12       IN      PTR     [master1.openshift.podX.io](http://master1.openshift.podx.io/).
    13       IN      PTR     [master2.openshift.podX.io](http://master2.openshift.podx.io/).
    14       IN      PTR     [master3.openshift.podX.io](http://master3.openshift.podx.io/).
    ;
    11       IN      PTR     [bootstrap.openshift.podX.io](http://bootstrap.openshift.podx.io/).
    ;
    10       IN      PTR     [api.openshift.podX.io](http://api.openshift.podx.io/).
    10       IN      PTR     [api-int.openshift.podX.io](http://api-int.openshift.podx.io/).
    ;
    15       IN      PTR     [worker1.openshift.podX.io](http://worker1.openshift.podx.io/).
    16       IN      PTR     [worker2.openshift.podX.io](http://worker2.openshift.podx.io/).
    ;
    ...
    ```
    
5. Enable and start bind service
    
    ```bash
    systemctl enable named
    systemctl restart named
    systemctl status named
    ```
    
6. Allow firewall
    
    ```bash
    firewall-cmd --add-service=dns --zone=public --permanent
    firewall-cmd --reload
    firewall-cmd --list-services
    ```
    
7. Change DNS Resolver
    
    ```bash
    vim /etc/sysconfig/network-scripts/ifcfg-enp1s0
    ```
    
    ```bash
    ...
    DNS1="192.168.1.X0"
    ...
    ```
    
    ```bash
    systemctl restart NetworkManager
    ```
    
8. Verify DNS forward lookup
    
    ```bash
    for i in helper api api-int bootstrap master1 master2 master3 worker1 worker2
    do
    dig +noall +answer ${i}.openshift.podX.io
    done
    ```
    
9. Verify DNS reverse lookup
    
    ```bash
    for i in {10..16}
    do
    dig +noall +answer -x 192.168.1.${i}
    done
    ```
    

---

# Setup DHCP Server

1. Install dhcp server package
    
    ```bash
    dnf install dhcp-server -y
    ```
    
2. Configure dhcp leases
    
    ```bash
    mv /etc/dhcp/dhcpd.conf /etc/dhcp/dhcpd.conf.bak
    vim /etc/dhcp/dhcpd.conf
    ```
    
    ```bash
    ...
    ddns-update-style interim;
    ignore client-updates;
    authoritative;
    allow booting;
    allow bootp;
    allow unknown-clients;
    
    #internal subnet for my DHCP Server
    subnet 192.168.1.0 netmask 255.255.255.0 {
    range 192.168.1.X0 192.168.1.210;
    option domain-name-servers 192.168.1.X0;
    option routers 192.168.1.1;
    option broadcast-address 192.168.1.255;
    default-lease-time 600;
    max-lease-time 7200; 
    
    host [bootstrap.openshift.podX.io](http://bootstrap.openshift.podx.io/) { hardware ethernet 52:54:00:3c:8d:79; fixed-address 192.168.1.X1; option host-name "[bootstrap.openshift.podX.io](http://bootstrap.openshift.podx.io/)"; }
    
    host [master1.openshift.podX.io](http://master1.openshift.podx.io/) { hardware ethernet 52:54:00:71:35:38; fixed-address 192.168.1.X2; option host-name "[master1.openshift.podX.io](http://master1.openshift.podx.io/)"; }
    host [master2.openshift.podX.io](http://master2.openshift.podx.io/) { hardware ethernet 52:54:00:f3:9f:d2; fixed-address 192.168.1.X3; option host-name "[master2.openshift.podX.io](http://master2.openshift.podx.io/)"; }
    host [master3.openshift.podX.io](http://master3.openshift.podx.io/) { hardware ethernet 52:54:00:b4:e7:d7; fixed-address 192.168.1.X4; option host-name "[master3.openshift.podX.io](http://master3.openshift.podx.io/)"; }
    
    host [worker1.openshift.podX.io](http://worker1.openshift.podx.io/) { hardware ethernet 52:54:00:b2:0c:39; fixed-address 192.168.1.X5; option host-name "[worker1.openshift.podX.io](http://worker1.openshift.podx.io/)"; }
    host [worker2.openshift.podX.io](http://worker2.openshift.podx.io/) { hardware ethernet 52:54:00:43:28:10; fixed-address 192.168.1.X6; option host-name "[worker2.openshift.podX.io](http://worker2.openshift.podx.io/)"; }
    
    deny unknown-clients; 
    
    #IP of PXE Server
    next-server 192.168.1.X0;
    if exists user-class and option user-class = "iPXE" {
    
    filename "[http://helper.openshift.podX.io:8080/boot.ipxe](http://helper.openshift.podx.io:8080/boot.ipxe)";
    
    } else {
    
    filename "undionly.kpxe";
    
    }
    }
    ...
    ```
    
3. Restart and enable dhcp service
    
    ```bash
    systemctl enable dhcpd
    systemctl restart dhcpd
    systemctl status dhcpd
    ```
    
4. Allow firewall
    
    ```bash
    firewall-cmd --add-service=dhcp --permanent
    firewall-cmd --reload
    firewall-cmd --list-services
    ```
    

---

# Install TFTP Server

1. Install tftp server package
    
    ```bash
    dnf install tftp-server ipxe-bootimgs -y
    ```
    
2. Create the standard tree for the TFTP server and link the image
    
    ```bash
    mkdir -p /var/lib/tftpboot
    ln -s /usr/share/ipxe/undionly.kpxe /var/lib/tftpboot
    ```
    
3. Start and enable TFTP Service
    
    ```bash
    systemctl enable tftp
    systemctl start tftp
    systemctl status tftp
    ```
    
4. Allow firewall
    
    ```bash
    firewall-cmd --permanent --add-service=tftp
    firewall-cmd --reload
    firewall-cmd --list-services
    ```
    

---

# Configure HAProxy and Rsyslog

1. Install packages
    
    ```bash
    dnf install -y haproxy rsyslog
    ```
    
2. Configure HAProxy
    
    ```bash
    mv /etc/haproxy/haproxy.cfg /etc/haproxy/haproxy.cfg.bak
    vim /etc/haproxy/haproxy.cfg
    ```
    
    ```bash
    ...
    global
    log         127.0.0.1 local2
    chroot      /var/lib/haproxy
    pidfile     /var/run/haproxy.pid
    maxconn     4000
    user        haproxy
    group       haproxy
    daemon
    
    # turn on stats unix socket
    stats socket /var/lib/haproxy/stats
    
    # utilize system-wide crypto-policies
    ssl-default-bind-ciphers PROFILE=SYSTEM
    ssl-default-server-ciphers PROFILE=SYSTEM
    
    defaults
    mode                    http
    log                     global
    option                  httplog
    option                  dontlognull
    option http-server-close
    option forwardfor       except 127.0.0.0/8
    option                  redispatch
    retries                 3
    timeout http-request    10s
    timeout queue           1m
    timeout connect         10s
    timeout client          1m
    timeout server          1m
    timeout http-keep-alive 10s
    timeout check           10s
    maxconn                 3000
    
    #Frontend
    frontend openshift-api-server
    bind [api.openshift.podX.io:6443](http://api.openshift.podx.io:6443/)
    default_backend openshift-api-server
    mode tcp
    option tcplog
    
    frontend machine-config-server
    bind [api-int.openshift.podX.io:22623](http://api-int.openshift.podx.io:22623/)
    default_backend machine-config-server
    mode tcp
    option tcplog
    
    frontend ingress-http
    bind *:80
    default_backend ingress-http
    mode tcp
    option tcplog
    
    frontend ingress-https
    bind *:443
    default_backend ingress-https
    mode tcp
    option tcplog 
    
    #Backend
    backend openshift-api-server
    balance source
    mode tcp
    server [bootstrap.openshift.podX.io](http://bootstrap.openshift.podx.io/) 192.168.1.X1:6443 check
    server [master1.openshift.podX.io](http://master1.openshift.podx.io/) 192.168.1.X2:6443 check
    server [master2.openshift.podX.io](http://master2.openshift.podx.io/) 192.168.1.X3:6443 check
    server [master3.openshift.podX.io](http://master3.openshift.podx.io/) 192.168.1.X4:6443 check
    
    backend machine-config-server
    balance source
    mode tcp
    server [bootstrap.openshift.podX.io](http://bootstrap.openshift.podx.io/) 192.168.1.X1:22623 check
    server [master1.openshift.podX.io](http://master1.openshift.podx.io/) 192.168.1.X2:22623 check
    server [master2.openshift.podX.io](http://master2.openshift.podx.io/) 192.168.1.X3:22623 check
    server [master3.openshift.podX.io](http://master3.openshift.podx.io/) 192.168.1.X4:22623 check
    
    backend ingress-http
    balance source
    mode tcp
    server [worker1.openshift.podX.io](http://worker1.openshift.podx.io/) 192.168.1.X5:80 check
    server [worker2.openshift.podX.io](http://worker2.openshift.podx.io/) 192.168.1.X6:80 check
    
    backend ingress-https
    balance source
    mode tcp
    server [worker1.openshift.podX.io](http://worker1.openshift.podx.io/) 192.168.1.X5:443 check
    server [worker2.openshift.podX.io](http://worker2.openshift.podx.io/) 192.168.1.X6:443 check
    ...
    ```
    
3. Enable haproxy log
    
    ```bash
    vim /etc/rsyslog.conf
    
    ...
    module(load="imudp") # needs to be done just once
    input(type="imudp" port="514")
    ...
    
    ```
    
    ```bash
    vim /etc/rsyslog.d/haproxy.conf
    
    ...
    local2.*    /var/log/haproxy.log
    ...
    ```
    
4. Setsebool to allow haproxy socket to open on any port
    
    ```bash
    setsebool -P haproxy_connect_any=1
    ```
    
5. Enable and restart haproxy & rsyslog
    
    ```bash
    systemctl enable haproxy
    systemctl restart haproxy
    systemctl status haproxy
    ```
    
    ```bash
    systemctl enable rsyslog
    systemctl restart rsyslog
    systemctl status rsyslog
    ```
    
6. Allow firewall
    
    ```bash
    firewall-cmd --permanent --add-service http
    firewall-cmd --permanent --add-service https
    firewall-cmd --permanent --add-port 6443/tcp
    firewall-cmd --permanent --add-port 22623/tcp
    firewall-cmd --reload
    firewall-cmd --list-services
    firewall-cmd --list-ports
    ```
    

---

# Setup Matchbox

1. Download Matchbox and move matchbox binary
    
    ```bash
    curl -LO [https://github.com/poseidon/matchbox/releases/download/v0.8.3/matchbox-v0.8.3-linux-amd64.tar.gz](https://github.com/poseidon/matchbox/releases/download/v0.8.3/matchbox-v0.8.3-linux-amd64.tar.gz)
    tar xvzf matchbox-v0.8.3-linux-amd64.tar.gz
    ```
    
    ```bash
    cd matchbox-v0.8.3-linux-amd64
    cp matchbox /usr/local/bin/
    matchbox --version
    ```
    
2. Create matchbox user for matchbox service
    
    ```bash
    useradd -U matchbox
    cat /etc/passwd | grep matchbox
    ```
    
3. Create configuration directory for matchbox
    
    ```bash
    mkdir -p /var/lib/matchbox/{assets,groups,ignition,profiles}
    chown -R matchbox:matchbox /var/lib/matchbox
    ls /var/lib/matchbox/
    ```
    
4. Copy, enable, and start matchbox service
    
    ```bash
    cp contrib/systemd/matchbox-local.service /etc/systemd/system/matchbox.service
    
    systemctl daemon-reload
    systemctl enable matchbox
    systemctl restart matchbox
    systemctl status matchbox
    ```
    
5. Download Fedora CoreOS
    
    ```bash
    cd /var/lib/matchbox/assets
    
    wget [https://builds.coreos.fedoraproject.org/prod/streams/stable/builds/34.20211031.3.0/x86_64/fedora-coreos-34.20211031.3.0-live-kernel-x86_64](https://builds.coreos.fedoraproject.org/prod/streams/stable/builds/34.20211031.3.0/x86_64/fedora-coreos-34.20211031.3.0-live-kernel-x86_64) -O fcos-kernel
    wget [https://builds.coreos.fedoraproject.org/prod/streams/stable/builds/34.20211031.3.0/x86_64/fedora-coreos-34.20211031.3.0-live-kernel-x86_64.sig](https://builds.coreos.fedoraproject.org/prod/streams/stable/builds/34.20211031.3.0/x86_64/fedora-coreos-34.20211031.3.0-live-kernel-x86_64.sig) -O fcos-kernel.sig
    wget [https://builds.coreos.fedoraproject.org/prod/streams/stable/builds/34.20211031.3.0/x86_64/fedora-coreos-34.20211031.3.0-live-initramfs.x86_64.img](https://builds.coreos.fedoraproject.org/prod/streams/stable/builds/34.20211031.3.0/x86_64/fedora-coreos-34.20211031.3.0-live-initramfs.x86_64.img) -O fcos-initramfs.img
    wget [https://builds.coreos.fedoraproject.org/prod/streams/stable/builds/34.20211031.3.0/x86_64/fedora-coreos-34.20211031.3.0-live-initramfs.x86_64.img.sig](https://builds.coreos.fedoraproject.org/prod/streams/stable/builds/34.20211031.3.0/x86_64/fedora-coreos-34.20211031.3.0-live-initramfs.x86_64.img.sig) -O fcos-initramfs.img.sig
    wget [https://builds.coreos.fedoraproject.org/prod/streams/stable/builds/34.20211031.3.0/x86_64/fedora-coreos-34.20211031.3.0-live-rootfs.x86_64.img](https://builds.coreos.fedoraproject.org/prod/streams/stable/builds/34.20211031.3.0/x86_64/fedora-coreos-34.20211031.3.0-live-rootfs.x86_64.img) -O fcos-rootfs.img
    wget [https://builds.coreos.fedoraproject.org/prod/streams/stable/builds/34.20211031.3.0/x86_64/fedora-coreos-34.20211031.3.0-live-rootfs.x86_64.img.sig](https://builds.coreos.fedoraproject.org/prod/streams/stable/builds/34.20211031.3.0/x86_64/fedora-coreos-34.20211031.3.0-live-rootfs.x86_64.img.sig) -O fcos-rootfs.img.sig
    ```
    
6. Create Matchbox Bootstrap profiles
    
    ```bash
    vim /var/lib/matchbox/profiles/bootstrap.json
    ```
    
    ```bash
    ...
    {
    "id": "bootstrap",
    "name": "OKD 4.9 - Bootstrap",
    "ignition_id": "bootstrap.ign",
    "boot": {
    "kernel": "/assets/fcos-kernel",
    "initrd": ["/assets/fcos-initramfs.img"],
    "args": [
    "ip=dhcp",
    "rd.neednet=1",
    "console=tty0",
    "console=ttyS0",
    "coreos.inst=yes",
    "coreos.inst.install_dev=/dev/vda",
    "coreos.inst.ignition_url=http://helper.openshift.podX.io:8080/ignition?mac=${mac:hexhyp}",
    "coreos.live.rootfs_url=http://helper.openshift.podX.io:8080/assets/fcos-rootfs.img"
    ]
    }
    }
    ...
    ```
    
7. Create Matchbox Master profiles
    
    ```bash
    vim /var/lib/matchbox/profiles/master.json
    ```
    
    ```bash
    ...
    {
    "id": "master",
    "name": "OKD 4.9 - Master",
    "ignition_id": "master.ign",
    "boot": {
    "kernel": "/assets/fcos-kernel",
    "initrd": ["/assets/fcos-initramfs.img"],
    "args": [
    "ip=dhcp",
    "rd.neednet=1",
    "console=tty0",
    "console=ttyS0",
    "coreos.inst=yes",
    "coreos.inst.install_dev=/dev/vda",
    "coreos.inst.ignition_url=http://helper.openshift.podX.io:8080/ignition?mac=${mac:hexhyp}",
    "coreos.live.rootfs_url=http://helper.openshift.podX.io:8080/assets/fcos-rootfs.img"
    ]
    }
    }
    ...
    ```
    
8. Create Matchbox Worker profiles
    
    ```bash
    vim /var/lib/matchbox/profiles/worker.json
    ```
    
    ```bash
    ...
    {
    "id": "worker",
    "name": "OKD 4.9 - worker",
    "ignition_id": "worker.ign",
    "boot": {
    "kernel": "/assets/fcos-kernel",
    "initrd": ["/assets/fcos-initramfs.img"],
    "args": [
    "ip=dhcp",
    "rd.neednet=1",
    "console=tty0",
    "console=ttyS0",
    "coreos.inst=yes",
    "coreos.inst.install_dev=/dev/vda",
    "coreos.inst.ignition_url=http://helper.openshift.podX.io:8080/ignition?mac=${mac:hexhyp}",
    "coreos.live.rootfs_url=http://helper.openshift.podX.io:8080/assets/fcos-rootfs.img"
    ]
    }
    }
    ...
    ```
    
9. Create groups for bootstrap node
    
    ```bash
    cat <<EOF >> /var/lib/matchbox/groups/bootstrap.json
    {
    "id": "bootstrap",
    "name": "OKD 4.9 - Bootstrap server",
    "profile": "bootstrap",
    "selector": {
    "mac": "52:54:00:ac:80:38"
    }
    }
    EOF
    ```
    
10. Create groups for master node
    
    ```bash
    cat <<EOF >> /var/lib/matchbox/groups/master1.json
    {
    "id": "master1",
    "name": "OKD 4.9 - Master 1",
    "profile": "master",
    "selector": {
    "mac": "52:54:00:cd:94:78"
    }
    }
    EOF
    
    cat <<EOF >> /var/lib/matchbox/groups/master2.json
    {
    "id": "master2",
    "name": "OKD 4.9 - Master 2",
    "profile": "master",
    "selector": {
    "mac": "52:54:00:39:2f:eb"
    }
    }
    EOF
    
    cat <<EOF >> /var/lib/matchbox/groups/master3.json
    {
    "id": "master3",
    "name": "OKD 4.9 - Master 3",
    "profile": "master",
    "selector": {
    "mac": "52:54:00:c7:78:27"
    }
    }
    EOF
    ```
    
11. Create groups for worker node
    
    ```bash
    cat <<EOF >> /var/lib/matchbox/groups/worker1.json
    {
    "id": "worker1",
    "name": "OKD 4.9 - Worker 1",
    "profile": "worker",
    "selector": {
    "mac": "52:54:00:34:73:b6"
    }
    }
    EOF
    
    cat <<EOF >> /var/lib/matchbox/groups/worker2.json
    {
    "id": "worker2",
    "name": "OKD 4.9 - Worker 2",
    "profile": "worker",
    "selector": {
    "mac": "52:54:00:a7:31:12"
    }
    }
    EOF
    ```
    
12. Allow firewall and restart matchbox
    
    ```bash
    firewall-cmd --permanent --add-port 8080/tcp
    firewall-cmd --reload
    firewall-cmd --list-ports
    systemctl restart matchbox
    ```
    

---

# Provisioning OKD Cluster

1. Generate SSH Keypair
    
    ```bash
    ssh-keygen -t rsa -b 2048 -N '' -f /root/.ssh/id_rsa
    ```
    
2. Obtaining the installation program
    
    ```bash
    wget [https://github.com/openshift/okd/releases/download/4.9.0-0.okd-2022-02-12-140851/openshift-client-linux-4.9.0-0.okd-2022-02-12-140851.tar.gz](https://github.com/openshift/okd/releases/download/4.9.0-0.okd-2022-02-12-140851/openshift-client-linux-4.9.0-0.okd-2022-02-12-140851.tar.gz)
    tar -xvf openshift-client-linux-4.9.0-0.okd-2022-02-12-140851.tar.gz
    cp kubectl oc /usr/local/bin/
    oc version
    kubectl version
    ```
    
    ```bash
    wget [https://github.com/openshift/okd/releases/download/4.9.0-0.okd-2022-02-12-140851/openshift-install-linux-4.9.0-0.okd-2022-02-12-140851.tar.gz](https://github.com/openshift/okd/releases/download/4.9.0-0.okd-2022-02-12-140851/openshift-install-linux-4.9.0-0.okd-2022-02-12-140851.tar.gz)
    tar -xvf openshift-install-linux-4.9.0-0.okd-2022-02-12-140851.tar.gz
    cp openshift-install /usr/local/bin/
    openshift-install version
    ```
    
3. Create a directory for okd config
    
    ```bash
    rm -rf /root/okd-config
    mkdir /root/okd-config
    cd /root/okd-config
    ```
    
4. Create installation configuration file
    
    ```bash
    key=cat ~/.ssh/id_rsa.pub
    tee -a install-config.yaml<<-EOF
    apiVersion: v1
    baseDomain: pod2.io
    compute:
    - hyperthreading: Enabled
      name: worker
      replicas: 0
    controlPlane:
      hyperthreading: Enabled
      name: master 
      replicas: 3
    metadata:
      name: openshift 
    networking:
      clusterNetwork:
      - cidr: 10.128.0.0/14 
        hostPrefix: 23 
      networkType: OVNKubernetes
      serviceNetwork: 
      - 172.30.0.0/16
    platform:
      none: {} 
    pullSecret: '{"auths":{"fake":{"auth": "bar"}}}' 
    sshKey: $key
    EOF
    ```
    
    ```bash
    cp install-config.yaml install-config.yaml.bak
    ```
    
5. Creating the Kubernetes manifest
    
    ```bash
    openshift-install create manifests --dir=/root/okd-config
    ```
    
6. Disable master schedulable
    
    ```bash
    vim manifests/cluster-scheduler-02-config.yml
    ```
    
    ```bash
    ...
    mastersSchedulable: false
    ...
    ```
    
7. Creating the ignition configuration files
    
    ```bash
    openshift-install create ignition-configs --dir=/root/okd-config
    tree
    ```
    
8. Copy ignition files to matchbox directory
    
    ```bash
    rm -rf /var/lib/matchbox/ignition/*.ign
    cp /root/okd-config/*.ign /var/lib/matchbox/ignition
    chown -R matchbox:matchbox /var/lib/matchbox
    chmod o+r /var/lib/matchbox/ignition/*.ign
    ls -l /var/lib/matchbox/ignition/
    ```
    
9. Power on bootstrap, master, and worker nodes
10. Check bootstrapping progress
    
    ```bash
    openshift-install --dir=/root/okd-config wait-for bootstrap-complete --log-level=info
    ```
    
11. Remote bootstrap from haproxy and restart haproxy
    
    ```bash
    vim /etc/haproxy/haproxy.cfg
    ```
    
    ```bash
    ...
    server [bootstrap.openshift.podX.io](http://bootstrap.openshift.podx.io/) 10.60.60.4:6443 check
    server [bootstrap.openshift.podX.io](http://bootstrap.openshift.podx.io/) 10.60.60.4:22623 check
    ...
    ```
    
    ```bash
    systemctl restart haproxy
    ```
    
12. Check installation progress after bootstrap is complete
    
    ```bash
    openshift-install --dir=/root/okd-config wait-for install-complete --log-level=debug
    ```
    
    > Check also node and pod status
    > 
    
13. Access cluster from CLI
    
    ```bash
    YOUR_PASSWORD=cat okd-config/auth/kubeadmin-password
    oc login -u kubeadmin -p ${YOUR_PASSWORD} [https://api.openshift.podX.io:6443](https://api.openshift.podx.io:6443/)
    ```
    
14. Check user
    
    ```bash
    oc whoami
    ```
    
15. Approving CSR
    
    ```bash
    oc get csr
    oc get csr | grep Pending | awk '{print $1}' | xargs oc adm certificate approve
    ```
