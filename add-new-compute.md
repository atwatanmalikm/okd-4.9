# Setup DNS Server
1\. Edit forward zone file
```bash
vim /var/named/dynamic/forward.db
```
```bash
...
$TTL 1W
@       IN      SOA     ns1.openshift.podX.io. root (
2020092301      ; serial
3H              ; refresh (3 hours)
30M             ; retry (30 minutes)
2W              ; expiry (2 weeks)
1W )            ; minimum (1 week)
IN      NS      ns1.openshift.podX.io.
IN      MX 10   smtp.openshift.podX.io.
;
;
ns1     IN      A       192.168.1.X0
smtp    IN      A       192.168.1.X0
;
helper  IN      A       192.168.1.X0
;
; The api points to the IP of your load balancer
api             IN      A       192.168.1.X0
api-int         IN      A       192.168.1.X0
;
; The wildcard also points to the load balancer
*.apps          IN      A       192.168.1.X0
;
; Create entry for the bootstrap host
bootstrap       IN      A       192.168.1.X1
;
; Create entries for the master hosts
master1         IN      A       192.168.1.X2
master2         IN      A       192.168.1.X3
master3         IN      A       192.168.1.X4
;
; Create entries for the worker hosts
worker1         IN      A       192.168.1.X5
worker2         IN      A       192.168.1.X6
worker3         IN      A       192.168.1.X7
;
...
```
    
2\. Edit reverse zone file
```bash
vim /var/named/dynamic/reverse.db
```
```bash
...
$TTL 1W
@       IN      SOA     ns1.openshift.podX.io. root (
2020092301      ; serial
3H              ; refresh (3 hours)
30M             ; retry (30 minutes)
2W              ; expiry (2 weeks)
1W )            ; minimum (1 week)
IN      NS      ns1.openshift.podX.io.
;
X2       IN      PTR     master1.openshift.podX.io.
X3       IN      PTR     master2.openshift.podX.io.
X4       IN      PTR     master3.openshift.podX.io.
;
X1       IN      PTR     bootstrap.openshift.podX.io.
;
X0       IN      PTR     api.openshift.podX.io.
X0       IN      PTR     api-int.openshift.podX.io.
;
X5       IN      PTR     worker1.openshift.podX.io.
X6       IN      PTR     worker2.openshift.podX.io.
X7       IN      PTR     worker3.openshift.podX.io.
;
...
```
    
3\. Restart bind service
```bash
systemctl restart named
systemctl status named
```
    
4\. Verify DNS forward lookup
```bash
for i in helper api api-int bootstrap master1 master2 master3 worker1 worker2 worker3
do
dig +noall +answer ${i}.openshift.podX.io
done
```
    
5\. Verify DNS reverse lookup
```bash
for i in {X0..X7}
do
dig +noall +answer -x 192.168.1.${i}
done
```

---

# Setup DHCP Server

1\. Edit dhcp leases
```bash
vim /etc/dhcp/dhcpd.conf
```
```bash
...
ddns-update-style interim;
ignore client-updates;
authoritative;
allow booting;
allow bootp;
allow unknown-clients;

subnet 192.168.1.0 netmask 255.255.255.0 {
range 192.168.1.X0 192.168.1.210;
option domain-name-servers 192.168.1.X0;
option routers 192.168.1.1;
option broadcast-address 192.168.1.255;
default-lease-time 600;
max-lease-time 7200;

host bootstrap.openshift.podX.io { hardware ethernet 52:54:00:e8:af:8e; fixed-address 192.168.1.X1; option host-name "bootstrap.openshift.podX.io"; }
host master1.openshift.podX.io { hardware ethernet 52:54:00:c6:1a:8e; fixed-address 192.168.1.X2; option host-name "master1.openshift.podX.io"; }
host master2.openshift.podX.io { hardware ethernet 52:54:00:63:13:b3; fixed-address 192.168.1.X3; option host-name "master2.openshift.podX.io"; }
host master3.openshift.podX.io { hardware ethernet 52:54:00:57:f7:51; fixed-address 192.168.1.X4; option host-name "master3.openshift.podX.io"; }
host worker1.openshift.podX.io { hardware ethernet 52:54:00:60:9b:71; fixed-address 192.168.1.X5; option host-name "worker1.openshift.podX.io"; }
host worker2.openshift.podX.io { hardware ethernet 52:54:00:46:92:a8; fixed-address 192.168.1.X6; option host-name "worker2.openshift.podX.io"; }
host worker3.openshift.podX.io { hardware ethernet 52:54:00:db:95:87; fixed-address 192.168.1.X7; option host-name "worker3.openshift.podX.io"; }

deny unknown-clients;
# IP of PXE Server
next-server 192.168.1.X0;
filename "http://helper.openshift.podX.io:8080/boot.ipxe";
}
...
```
    
2\. Restart dhcp service
```bash
systemctl restart dhcpd
systemctl status dhcpd
```

---

# Configure HAProxy and Rsyslog

1\. Configure HAProxy
```bash
vim /etc/haproxy/haproxy.cfg
```
```bash
...
global
log         127.0.0.1 local2
chroot      /var/lib/haproxy
pidfile     /var/run/haproxy.pid
maxconn     4000
user        haproxy
group       haproxy
daemon
# turn on stats unix socket
stats socket /var/lib/haproxy/stats
# utilize system-wide crypto-policies
ssl-default-bind-ciphers PROFILE=SYSTEM
ssl-default-server-ciphers PROFILE=SYSTEM

defaults
    mode                    http
    log                     global
    option                  httplog
    option                  dontlognull
    option http-server-close
    option forwardfor       except 127.0.0.0/8
    option                  redispatch
    retries                 3
    timeout http-request    10s
    timeout queue           1m
    timeout connect         10s
    timeout client          1m
    timeout server          1m
    timeout http-keep-alive 10s
    timeout check           10s
    maxconn                 3000

# Frontend
frontend openshift-api-server
    bind api.openshift.podX.io:6443
    default_backend openshift-api-server
    mode tcp
    option tcplog
frontend machine-config-server
    bind api-int.openshift.podX.io:22623
    default_backend machine-config-server
    mode tcp
    option tcplog
frontend ingress-http
    bind *:80
    default_backend ingress-http
    mode tcp
    option tcplog
frontend ingress-https
    bind *:443
    default_backend ingress-https
    mode tcp
    option tcplog

# Backend
backend openshift-api-server
    balance source
    mode tcp
    #server bootstrap.openshift.podX.io 192.168.1.X1:6443 check
    server master1.openshift.podX.io 192.168.1.X2:6443 check
    server master2.openshift.podX.io 192.168.1.X3:6443 check
    server master3.openshift.podX.io 192.168.1.X4:6443 check
backend machine-config-server
    balance source
    mode tcp
    #server bootstrap.openshift.podX.io 192.168.1.X1:22623 check
    server master1.openshift.podX.io 192.168.1.X2:22623 check
    server master2.openshift.podX.io 192.168.1.X3:22623 check
    server master3.openshift.podX.io 192.168.1.X4:22623 check
backend ingress-http
    balance source
    mode tcp
    server worker1.openshift.podX.io 192.168.1.X5:80 check
    server worker2.openshift.podX.io 192.168.1.X6:80 check
    server worker3.openshift.podX.io 192.168.1.X7:80 check
backend ingress-https
    balance source
    mode tcp
    server worker1.openshift.podX.io 192.168.1.X5:443 check
    server worker2.openshift.podX.io 192.168.1.X6:443 check
    server worker3.openshift.podX.io 192.168.1.X7:443 check
    ...
```
    
2\. Restart haproxy & rsyslog
```bash
systemctl restart haproxy
systemctl status haproxy
```

---

# Setup Matchbox

1. Create groups for new node
    
```bash
cat <<EOF >> /var/lib/matchbox/groups/worker3.json
{
"id": "worker3",
"name": "OKD 4.9 - Worker 3",
"profile": "worker",
"selector": {
"mac": "52:54:00:db:95:87"
}
}
EOF
```

---

# Apply Configuration

1\. Enter to directory okd config
```bash
cd /root/okd-config
```
    
2\. Creating the ignition configuration files
    
```bash
openshift-install create ignition-configs --dir=/root/okd-config
tree
```
    
3\. Copy ignition files to matchbox directory
    
```bash
rm -rf /var/lib/matchbox/ignition/*.ign
cp /root/okd-config/*.ign /var/lib/matchbox/ignition
chown -R matchbox:matchbox /var/lib/matchbox
chmod o+r /var/lib/matchbox/ignition/*.ign
ls -l /var/lib/matchbox/ignition/
```
    
4\. Power on new worker node
    
5\. Monitor list node    
```bash
watch -n oc get node
```
     
6\. Approving CSR
```bash
oc get csr
oc get csr | grep Pending | awk '{print $1}' | xargs oc adm certificate approve
```

7\. Test create pod using the following manifest

`Workloads - Pods - Create Pod`

```
...
apiVersion: v1
kind: Pod
metadata:
  name: test-worker3
  labels:
    app: test-worker3
  namespace: default
spec:
  selector:
    matchLabels:
      kubernetes.io/hostname: worker3.openshift.podX.io
  containers:
    - name: test-worker3
      image: 'nginx'
      ports:
        - containerPort: 80
...
```